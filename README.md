1. Data was obtained in three steps:


    1) I scrapped Twitter and collected dataset(> 100000 examples) of tweets in Russian that contain a word 'Трамп'
    
    2) I selected a small subset of tweets (approximately 500) and labeled them manually
    
    It's a TEST dataset. You can find it in data\test.csv
    
    (You may find a code for annotation in annotation folder)
    
    3) For creating a training dataset I used some sort of distant supervision.
    
    I used simple sentiment-lexicon(https://github.com/text-machine-lab/sentimental) 
    to create sentiment scores for tweets that I obtained in step 1
    
    Then I selected subset(approximately 20000) of tweets that 
    
    a)That are not in test
    b)That have not zero sentiment score
    
    Then, according to sentiment score, I labeled this subset
    
    It's my TRAIN dataset

2. I choose AUC-ROC as my evaluation metric, because of its relative robustness to class imbalance

3. I trained a few baseline classificators - in order to understand the situation better:
    
    You can find implementation in baselines.py
    
    
    a)Modification of 'Baselines and Bigrams: Simple, Good Sentiment and Topic Classification'
    (http://www.aclweb.org/anthology/P12-2018)

    It achieved 87.9% ROC-AUC
    
    b)LightGbm + word and char ngram features

    It achieved 86.2% ROC-AUC
    
    c) Logistic Regression + word and char ngram features

    It achieved 89% ROC-AUC
    
5. As a solution I trained a Deep Learning model:

    
    Simple LSTM + some (spatial + ordinary) dropout
    
    I used the last fasttext for creating an embedding layer.

    Also, I averaged predictions across several epochs - it provided a slight improvement.

    It achieved approximately 90% ROC-AUC

    The corresponding Accuracy is approximately 90%
    
6. You may find the implementation of DL model + code for training in 

 
    j_try_simple_lstm.ipynb
    
7. Preprocessing:


    It consisted of only tokenization followed by lemmatization

    I used pymystem3 for these purposes

    Code for text preprocessing is in text_normalization.py
    
8. Predictions of the best model(and other models as well) are in predictions/

9. The plot of ROC AUC curve is in roc_auc.png

10. Further steps. I had little time to work on this task so I want to share some ideas that can help improve quality:

    1. Tune parameters of DL model. Current is a rough guess

    2. Use some sort of learning rate annealing

    3. Use more powerful language model (instead of plain word embedding)

    For instance realization of 'Regularizing and Optimizing LSTM Language Models'(https://arxiv.org/pdf/1708.02182.pdf)

    4. Try implement ideas from this paper: https://arxiv.org/abs/1801.06146
    (https://arxiv.org/abs/1801.06146)
    (Fine tuning of language model)
