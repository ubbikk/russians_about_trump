import re
from collections import Counter

import pandas as pd
from sentimental import Sentimental

url_pattern = 'https?://[^\s]+'
emoji_pattern = '<emoji: [^>]+>'


def pretify_tweet(s):
    s = s.lower().strip()
    s = re.sub(url_pattern, ' ', s)
    # s = '\n'.join(textwrap.wrap(s, width=70))
    return s


def preprocess_v1(df):
    df['prettified'] = df['tweet'].apply(pretify_tweet)
    df = df.drop_duplicates('prettified')
    return df


def filter_with_emoji(df, col='prettified'):
    return df[df[col].apply(lambda s: 'emoji:' in s)]


def extract_emojis(s):
    res = re.findall(emoji_pattern, s)
    res = [x[8: -1] for x in res]
    return res


def add_sentiment_based_on_emoji(df, col='prettified'):
    df['emojis'] = df[col].apply(extract_emojis).apply(set)


def most_common_emojis(df):
    ll = []
    for s in df['emojis']:
        ll += list(s)
    return Counter(ll)


def filter_with_particular_emoji(df, e):
    return df[df['emojis'].apply(lambda s: e in s)]


if __name__ == '__main__':
    df = pd.read_csv('/home/ubik/projects/wr/russian_trump/twint_data_2018-05-01_orig.csv')
    preprocess_v1(df)
    df = df[['id', 'prettified']]
    df.to_csv('/home/ubik/projects/wr/russian_trump/twint_data_2018-05-01_prettified.csv', index=False)

    sent = Sentimental()
    df['s'] = df['prettified'].apply(sent.analyze)
    for col in ['score', 'positive', 'negative', 'comparative']:
        df[col] = df.s.apply(lambda s: s[col])

    del df['s']

    bl = df[df['score'] != 0]
    bl.to_csv('/home/ubik/projects/wr/russian_trump/twint_data_2018-05-01_with_sentiment.csv', index=False)

    bl['l'] = bl.prettified.apply(len)
    bl = bl[bl.l < 125]
    bl.to_csv('/home/ubik/projects/wr/russian_trump/twint_data_2018-05-01_with_sentiment_short.csv', index=False)
