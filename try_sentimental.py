import pandas as pd
from sentimental import Sentimental

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_colwidth', 5000)

sent = Sentimental()

# sentence = 'Today is a good day!'
# result = sent.analyze(sentence)

df = pd.read_csv('/home/ubik/projects/wr/russian_trump/twint_data_2018-04-01_short.csv')
bl = df[['id', 'prettified']][:1000]

bl['s'] = bl['prettified'].apply(sent.analyze)
for col in ['score', 'positive', 'negative', 'comparative']:
    bl[col] = bl.s.apply(lambda s: s[col])

del bl['s']
