from pymystem3 import Mystem
from tqdm import tqdm

tqdm.pandas()


def analyze(text, mystem):
    analyzed = mystem.analyze(text)
    tokenized = []
    lemmatized = []
    for x in analyzed:
        tokenized.append(x['text'])
        if 'analysis' in x and len(x['analysis']) != 0:
            a = x['analysis'][0]['lex']
            lemmatized.append(a)
        else:
            lemmatized.append(x['text'])

    return list(zip(tokenized, lemmatized))


def normalize_text(txt, nlp, lemmatization, lower):
    txt = analyze(txt, nlp)
    normalized = list()
    for word in txt:
        if lemmatization:
            val = word[1].strip()
            normalized.append(val)
        else:
            val = word[0].strip()
            if lower:
                val = val.lower()
            normalized.append(val)
    return " ".join(normalized)


def normalize_text_df(df, col):
    nlp = Mystem()
    df['normalized'] = df[col].progress_apply(lambda s: normalize_text(s, nlp, False, False))
    df['normalized_lemmatized'] = df[col].progress_apply(lambda s: normalize_text(s, nlp, True, False))
    df['normalized_lower'] = df[col].progress_apply(lambda s: normalize_text(s, nlp, False, True))
