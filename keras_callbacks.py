import numpy as np
from keras.callbacks import Callback
from sklearn.metrics import roc_auc_score, log_loss


class SimpleRocAucCallback(Callback):

    def __init__(self, train, test, batch_size=2048):
        super(SimpleRocAucCallback, self).__init__()
        self.train_data = train[0]
        self.train_target = train[1]
        self.test_data = test[0]
        self.test_target = test[1]

        self.batch_size = batch_size

        self.train_probs = []
        self.test_probs = []
        self.global_test_probs = []

        self.performance = {'auc': [], 'logloss': []}

    def on_train_begin(self, logs={}):
        return

    def on_train_end(self, logs={}):
        return

    def on_epoch_begin(self, epoch, logs={}):
        return

    def on_epoch_end(self, epoch, logs={}):
        train_probs = self.model.predict(self.train_data, batch_size=self.batch_size)
        self.train_probs.append(train_probs)

        test_probs = self.model.predict(self.test_data, batch_size=self.batch_size)
        self.test_probs.append(test_probs)
        print('\n')

        train_auc = roc_auc_score(self.train_target, train_probs)
        test_auc = roc_auc_score(self.test_target, test_probs)
        train_log_loss = log_loss(self.train_target, train_probs)
        test_log_loss = log_loss(self.test_target, test_probs)

        self.performance['auc'].append({'train': train_auc, 'test': test_auc})
        self.performance['logloss'].append({'train': train_log_loss, 'test': test_log_loss})

        print(f'Auc     {train_auc:.3f} / {test_auc:.3f}')
        print(f'LogLoss {train_log_loss:.3f} / {test_log_loss:.3f}')
        print('===========================\n')

    def on_batch_begin(self, batch, logs={}):
        return

    def on_batch_end(self, batch, logs={}):
        return

    def average_epochs_results(self, epochs):
        probs = np.array(self.test_probs)
        probs = np.array(probs[epochs])
        probs = probs.mean(axis=0)
        test_auc = roc_auc_score(self.test_target, probs)
        test_log_loss = log_loss(self.test_target, probs)
        return {'auc': test_auc, 'logloss': test_log_loss, 'probs': probs}
