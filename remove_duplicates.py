import difflib

from tqdm import tqdm


def similarity(a, b):
    return difflib.SequenceMatcher(a=a, b=b).ratio()


def process_df(df, col='prettified'):
    col_index = list(df.columns).index(col)
    sz = len(df)
    dups = set()
    for i in tqdm(range(sz)):
        for j in range(sz):
            if i < j:
                s = similarity(df.iloc[i, col_index], df.iloc[j, col_index])
                if s > 0.6:
                    dups.add(j)

    return df.drop(index=dups)
