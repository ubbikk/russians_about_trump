import re

import numpy as np
import pandas as pd
from sentimental import Sentimental
from tqdm import tqdm

from text_normalization import normalize_text_df

tqdm.pandas()

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_colwidth', 5000)

punct_pattern = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
url_pattern = 'https?://[^\s]+'


def shuffle_df(df, random_state=241):
    np.random.seed(random_state)
    return df.iloc[np.random.permutation(len(df))]


if __name__ == '__main__':
    fps = [
        '../data/trump_russian_2018-01-03__2018-03-30.csv',
        '../data/trump_russian_2018-03-31__2018_05_27.csv',
        '../data/trump_russian_2017.csv'
    ]

    df = pd.concat([pd.read_csv(fp) for fp in fps])
    df = df[['id', 'date', 'tweet']]

    # df=df[:1000]

    df.tweet = df.tweet.apply(lambda s: re.sub('@\w+', '', s))
    df.tweet = df.tweet.apply(lambda s: re.sub('#\w+', '', s))
    df.tweet = df.tweet.apply(lambda s: re.sub(url_pattern, ' ', s))
    df.tweet = df.tweet.apply(lambda s: re.sub(f'[{string.punctuation}]', ' ', s))

    normalize_text_df(df, 'tweet')
    df = df[df.normalized != '']

    sent = Sentimental()

    df['s'] = df['normalized_lemmatized'].apply(sent.analyze)
    for col in ['score', 'positive', 'negative', 'comparative']:
        df[col] = df.s.progress_apply(lambda s: s[col])

    del df['s']

    df['score_abs'] = df['score'].apply(abs)
    df = df[df['score_abs'] != 0]
    test = pd.read_csv('data/test.csv')
    test_ids = set(test['id'])
    df = df[df['id'].apply(lambda s: s not in test_ids)]
    train_df = df[:25_000]
    train_df['target'] = train_df.score.apply(lambda s: int(s > 0))
    cols = ['id', 'tweet', 'normalized',
            'normalized_lower', 'normalized_lemmatized', 'target']

    train_df = train_df[cols]
    train_df.to_csv('data/train.csv', index=False)
