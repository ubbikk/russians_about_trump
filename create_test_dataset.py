import re

import numpy as np
import pandas as pd
from tqdm import tqdm

from text_normalization import normalize_text_df

tqdm.pandas()

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_colwidth', 5000)

punct_pattern = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
url_pattern = 'https?://[^\s]+'


def shuffle_df(df, random_state=241):
    np.random.seed(random_state)
    return df.iloc[np.random.permutation(len(df))]


if __name__ == '__main__':
    import os

    print(os.getcwd())
    df = pd.read_csv('data/tweets_about_trump_with_raw_sentiment.csv')
    df = df[['id', 'date', 'tweet']]
    # df = shuffle_df(df)

    labeled = pd.read_csv('data/test_raw.csv', header=None)
    labeled.columns = ['id', 'target']
    labeled = labeled[labeled.target.apply(lambda s: s in ['p', 'n'])]
    labeled.target = labeled.target.apply(lambda s: int(s == 'p'))
    df = pd.merge(df, labeled, on='id')

    df = shuffle_df(df)

    df.tweet = df.tweet.apply(lambda s: re.sub('@\w+', '', s))
    df.tweet = df.tweet.apply(lambda s: re.sub('#\w+', '', s))
    df.tweet = df.tweet.apply(lambda s: re.sub(url_pattern, ' ', s))
    df.tweet = df.tweet.apply(lambda s: re.sub(f'[{string.punctuation}]', ' ', s))

    normalize_text_df(df, 'tweet')
    df.to_csv('data/test.csv', index=False)
