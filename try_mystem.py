from pymystem3 import Mystem

text = "Красивая мама красиво мыла раму!!"
mystem = Mystem()


def normalize(text, mystem):
    analyzed = mystem.analyze(text)
    tokenized = []
    lemmatized = []
    for x in analyzed:
        tokenized.append(x['text'])
        if 'analysis' in x:
            a = x['analysis'][0]['lex']
            lemmatized.append(a)
        else:
            lemmatized.append(x['text'])

    return list(zip(tokenized, lemmatized))
