import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import roc_curve, auc

train_fp = 'data/train.csv'
test_fp = 'data/test.csv'

test_results_fp = 'predictions/simple_lstm_solution.csv'


def split_into_train_val_test():
    train = pd.read_csv(train_fp)
    train, validation = train[:-5_000], train[-5_000:]
    test = pd.read_csv(test_fp)

    return train, validation, test


def find_best_cutoff_and_accuracy(y_true, probs):
    s = np.sort(probs)
    cutoffs = [(s[i] + s[i + 1]) / 2 for i in range(len(s) - 2)]
    best_cuttoff = None
    best_accuracy = -1
    for c in cutoffs:
        predictions = (probs > c).astype(int)
        accuracy = sum(predictions.values == y_true) / len(probs)
        if accuracy > best_accuracy:
            best_accuracy = accuracy
            best_cuttoff = c

    return best_cuttoff, best_accuracy


if __name__ == '__main__':
    preds = pd.read_csv(test_results_fp).positive

    train, validation, test = split_into_train_val_test()
    y_test = test.target

    fpr, tpr, threshold = roc_curve(y_test, preds)
    roc_auc = auc(fpr, tpr)

    best_cuttoff, best_accuracy = find_best_cutoff_and_accuracy(y_test, preds)

    plt.title('Receiver Operating Characteristic ')
    label = f'AUC={roc_auc:0.3f} (Accuracy={best_accuracy:0.3f})'
    plt.plot(fpr, tpr, 'b', label=label)
    plt.legend(loc='lower right')
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.show()
