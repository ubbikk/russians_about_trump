import os
import re
import textwrap

import pandas as pd

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_colwidth', 5000)

url_pattern = 'https?://[^\s]+'


def pretify_tweet(s):
    s = s.lower()
    s = re.sub(url_pattern, ' ', s)
    s = '\n'.join(textwrap.wrap(s, width=70))
    return s


# in_fp = sys.argv[1]
# out_fp = sys.argv[2]

in_fp = 'data/trump_russian_2018-03-31__2018_05_27.csv'
out_fp = 'data/res.csv'
col = 'tweet'


def load_words_to_skip():
    words_to_skip = open('annotation/words_to_skip.txt').readlines()
    words_to_skip = set([x.strip() for x in words_to_skip])
    return words_to_skip


def load_phrases_to_skip():
    words_to_skip = open('annotation/phrases_to_skip.txt').readlines()
    words_to_skip = set([x.strip() for x in words_to_skip])
    return words_to_skip


def should_skip(s):
    words_to_skip = load_words_to_skip()
    phrases_to_skip = load_phrases_to_skip()
    a = words_to_skip.intersection(set(s.split()))
    if len(a) > 0:
        return True
    for p in phrases_to_skip:
        if p in s:
            return True

    return False


df = pd.read_csv(in_fp)
col_index = list(df.columns).index(col)
id_index = list(df.columns).index('id')
if os.path.exists(out_fp):
    with open(out_fp) as f:
        start = len(f.readlines()) - 1
else:
    start = 0

while start < len(df):
    with open(out_fp, 'a') as f:
        tweet = df.iloc[start, col_index].lower()
        # print(tweet)
        # print('продлил санкции' in tweet)
        id_ = df.iloc[start, id_index]
        if should_skip(tweet):
            f.write(f'{id_},s\n')
            f.flush()
            start += 1
            continue
        print(id_)
        print()
        print(pretify_tweet(tweet))

        result = input()
        f.write(f'{id_},{result}\n')
        f.flush()
        start += 1
