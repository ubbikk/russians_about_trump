from collections import Counter

import numpy as np
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer


def _create_tokenizer_from_df(train, text_normalizer, max_features, field):
    list_sentences_train = train[field].fillna("_na_").apply(text_normalizer).values
    tokenizer = Tokenizer(num_words=max_features, filters='\t\n')
    tokenizer.fit_on_texts(list(list_sentences_train))

    return tokenizer


def _get_coefs(o):
    s = o.strip().split()
    word = ' '.join(s[:-300])
    arr = np.asarray(s[-300:], dtype='float32')
    return word, arr


def _create_glove_embeding_matrix(fp, max_features, word_index, embed_size, fasttext=False, init_with_zero=False):
    gen = open(fp)
    if fasttext:
        print('fastext {}'.format(next(gen)))
        embeddings_index = dict(_get_coefs(o) for o in gen)
        embeddings_index = {k: v for k, v in embeddings_index.items() if len(v) == embed_size}
    else:
        embeddings_index = dict(_get_coefs(*o.strip().split()) for o in gen)
    c = Counter([len(x) for x in embeddings_index.values()])
    print(c)
    nb_words = min(max_features, len(word_index) + 1)
    print('Init with zero {}'.format(init_with_zero))
    if init_with_zero:
        embedding_matrix = np.zeros((nb_words, embed_size))
    else:
        all_embs = np.stack(embeddings_index.values())
        emb_mean, emb_std = all_embs.mean(), all_embs.std()
        embedding_matrix = np.random.normal(emb_mean, emb_std, (nb_words, embed_size))

    unknown_words_counter = 0
    unknown_words = []
    for word, i in word_index.items():
        if i >= max_features:
            continue
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector
        else:
            unknown_words_counter += 1
            unknown_words.append(word)

    print('Unknown words = {}'.format(unknown_words_counter))
    print('Most freq unknown {}'.format(Counter(unknown_words).most_common(10)))
    return embedding_matrix


def _df_to_sequences(df, tokenizer, maxlen, text_normalizer, field):
    list_sentences = df[field].fillna("_na_").apply(text_normalizer).values

    list_tokenized_train = tokenizer.texts_to_sequences(list_sentences)
    data = pad_sequences(list_tokenized_train, maxlen=maxlen)

    return data


def preprocess_for_dl(train,
                      validation,
                      test,
                      field,
                      embeddings_fp,
                      max_len,
                      max_features,
                      embed_size,
                      fasttext=False,
                      init_with_zero=False,
                      text_normalizer=lambda s: s):
    tokenizer = _create_tokenizer_from_df(train, text_normalizer, max_features, field)
    embedding_matrix = _create_glove_embeding_matrix(embeddings_fp,
                                                     max_features,
                                                     tokenizer.word_index,
                                                     embed_size,
                                                     fasttext=fasttext,
                                                     init_with_zero=init_with_zero)

    train_data = _df_to_sequences(train, tokenizer, max_len, text_normalizer, field)
    validation_data = _df_to_sequences(validation, tokenizer, max_len, text_normalizer, field)
    test_data = _df_to_sequences(test, tokenizer, max_len, text_normalizer, field)

    additional_info = {'embedding_matrix': embedding_matrix, 'tokenizer': tokenizer}
    return train_data, validation_data, test_data, embedding_matrix
